import profileReducer, {actions} from "./profile-reducer";


let state = {
    postsData: [
        {id: 1, message: 'Hello, how are you?', likes: 15},
        {id: 2, message: 'It\'s my first post', likes: 20},
    ],
    profile: null,
    status: '',
    newPostText: ''
};

it('length of post should be incremented', () => {
    // 1. test data
    let action = actions.addPostActionCreator("hello there");

    // 2. action
    let newState = profileReducer(state, action);

    // 3. expectation
    expect(newState.postsData.length).toBe(3);
});

it('post message contains text that we expect', () => {
    // 1. test data
    let action = actions.addPostActionCreator("hello there");

    // 2. action
    let newState = profileReducer(state, action);

    // 3. expectation
    expect(newState.postsData[2].message).toBe("hello there");
});

it('length of post should be decremented after delete', () => {
    // 1. test data
    let action = actions.deletePost(1);

    // 2. action
    let newState = profileReducer(state, action);

    // 3. expectation
    expect(newState.postsData.length).toBe(1);
});

it('length of post should not be changed if id incorrect', () => {
    // 1. test data
    let action = actions.deletePost(100);

    // 2. action
    let newState = profileReducer(state, action);

    // 3. expectation
    expect(newState.postsData.length).toBe(2);
});