import {InferActionsTypes} from "./redux-store";


type DialogType = {
    id: number,
    name: string
}

type MessageType = {
    id: number,
    message: string
}

let initialState = {
    dialogsData: [
        {id: 1, name: 'Oleg'},
        {id: 2, name: 'Bulat'},
        {id: 3, name: 'Arthur'},
        {id: 4, name: 'Rem'},
        {id: 5, name: 'Max'},
        {id: 6, name: 'Tusyan'},
    ] as Array<DialogType>,
    messagesData: [
        {id: 1, message: 'Hi'},
        {id: 2, message: 'How are you?'},
        {id: 3, message: 'What\'s up?'},
    ] as Array<MessageType>,
};

const dialogsReducer = (state = initialState, action: ActionsType): InitialStateType => {
    switch (action.type) {
        case 'DIALOGS/ADD-MESSAGE':
            let newMessage = action.newMessageText;
            return {
                ...state,
                messagesData: [...state.messagesData, {id: 4, message: newMessage}]
            };
        default:
            return state;
    }
}

export const actions = {
    sendMessage: (newMessageText: string) => ({type: 'DIALOGS/ADD-MESSAGE', newMessageText} as const)
}

export default dialogsReducer;

export type InitialStateType = typeof initialState
type ActionsType = InferActionsTypes<typeof actions>