import React from 'react';
import css from './Post.module.css';


type PropsType = {
    message: string
    likes: number
}

const Post: React.FC<PropsType> = (props) => {
    return (
        <div className={css.item}>
            <img src='https://upl.stack.com/wp-content/uploads/2016/10/31105245/GSSI-Lab-at-IMG-Academy-STACK.jpg' />
            {props.message}
            <div>
                <span>Likes ♥ {props.likes}</span>
            </div>
        </div>
    );
}

export default Post;