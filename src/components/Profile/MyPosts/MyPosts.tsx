import React from 'react';
import css from './MyPosts.module.css';
import Post from './Post/Post';
import AddPostForm, {AddPostFormValuesType} from "./AddPostForm/AddPostForm";
import {PostType} from "../../../types/types";


export type MapPropsType = {
    postsData: Array<PostType>
}

export type DispatchPropsType = {
    addPost: (newPostText: string) => void
}

const MyPosts: React.FC<MapPropsType & DispatchPropsType> = props => {

    let postsElements = props.postsData
        .map(p => <Post key={p.id} message={p.message} likes={p.likes}/>);

    let onAddNewPost = (values: AddPostFormValuesType) => {
        props.addPost(values.newPostText);
    }

    return (
        <div className={css.postsBlock}>
            <h3>My posts</h3>
            <AddPostForm onSubmit={onAddNewPost}/>
            <div className={css.posts}>
                {postsElements}
            </div>
        </div>
    )
}

const MyPostsMemorized = React.memo(MyPosts);

export default MyPostsMemorized;