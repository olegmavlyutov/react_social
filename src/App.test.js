import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import ReactDOM from "react-dom";
import SocialMainApp from "./App";

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SocialMainApp />, div);
  ReactDOM.unmountComponentAtNode(div);
});
