import React from "react";


export function withSuspense<WrappedComponentProps>(WrappedComponent: React.ComponentType<WrappedComponentProps>) {
    return (props: WrappedComponentProps) => {
        return <React.Suspense fallback={<div>Loading...</div>}>
            <WrappedComponent {...props}/>
        </React.Suspense>

    }
}